package Election;
import java.util.Random;

/** This class is used to model election officers, which, after a random period
 *  of time, read a Vote from the sharedBuffer, which is then collated onto the
 *  tally of the corresponding candidate.
 * 
 * @author Adam R Nickalls, ID: 100088688
 * @version 1.0.
 */
public class Officer extends Thread{
    
    private Buffer sharedBuffer;
    public int officerID;
    
    
/** The Officer class constructor.
 * 
 * @param b  - [Buffer] This is the buffer that the object is associated to.
 * @param id - [int] The object identifier.
 */
    public Officer(Buffer b, int id){
        sharedBuffer = b;
        officerID = id;
    }
    
    
/** The main code to be executed for each Officer thread.
 */
    @Override
    public void run(){
        
        Random r = new Random();
        
        if(Election.totalTally == Election.numberOfVoters && sharedBuffer.isEmpty() == true){                
            while(Officer.activeCount() != 0){
                return;
            }
// When the required number of votes have been recorded, AND the buffer is empty
// return (end) all currently active Officer threads.            
        }

        sharedBuffer.acquire();
// The Officer thread now has sole access to the buffer that it is associated to.

        if(sharedBuffer.isEmpty() == true){
            sharedBuffer.release();

            try{
                sleep((r.nextInt(9)+1)*1000);
            }
            catch(InterruptedException iE){
                //System.out.println(iE);
            }
// If the buffer is empty, the mutual exclusion to the buffer is dropped and the
// thread sleeps for a random period of time (1 to 10 seconds).        
        }
        
        else{       
// Otherwise, as long as the buffer is not empty, read from it.
        
            for(Candidate currentElement : Election.candidates){
                if(sharedBuffer.buffer[sharedBuffer.bufferOUT].candidateID == currentElement.candidateID){
                    currentElement.tally++;
                    Vote v = sharedBuffer.removeItem();
                    System.out.println("VOTER: "+v.voteID+"\tCANDIDATE: "+currentElement.candidateID+"\tOFFICER: "+this.officerID+"\t[VOTE STORED]");
                    break;
                }
// Otherwise, the vote (at position bufferOUT) is read from the buffer. The 
// removeItem() method returns the Vote class object at the buffer's bufferOUT 
// position, so that the vote's details can be collected and used.
            }
            
            sharedBuffer.release();
// The mutual exclusion on the sharedBuffer is dropped. The current Officer 
// thread then sleeps for a random period of time (1 to 10 seconds).
            try{
                sleep((r.nextInt(9)+1)*1000);
            }
            catch(InterruptedException iE){
                //System.out.println(iE);
            }
        }
        run();
// Like the Booth threads, all Officer threads are in a collective loop until
// the specified number of votes have been registered.
    }
}