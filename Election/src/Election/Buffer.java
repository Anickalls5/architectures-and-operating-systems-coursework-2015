package Election;

/** This class creates a sharable buffer for use in the election simulation, the
 *  given Semaphore class is used to enable mutual exclusion on Buffer class
 *  instances.
 * 
 * @author Adam R Nickalls, ID: 100088688
 * @version 1.0.
 */
public class Buffer {
    
    public Vote[] buffer;
    public int bufferSize;
    public int bufferIN;
    public int bufferOUT;
    private Semaphore mutex = new Semaphore(1);
    

/** The Buffer class constructor.
 * 
 * @param size - [int] The size of the buffer.
 */    
    public Buffer(int size){
        bufferSize = size;
        buffer = new Vote[bufferSize];
        bufferIN = 0;
        bufferOUT = 0;
    }
    
    
/** This method checks if the invoking Buffer object is COMPLETELY empty, that 
 *  is, if all elements within the BUFFER array contain null.
 * 
 * @return - [boolean] Indicating whether the buffer is empty or not.
 */
    public boolean isEmpty(){
        
        int emptyCells = 0;
        
        for(int i = 0; i <= bufferSize - 1; i++){
            if(buffer[i] == null){
                emptyCells++;
            }
// For each of the elements in the buffer, check if it is null or not.            
        }
        
        if(emptyCells == bufferSize){
            return true;
            
// If there are no stored values within this buffer, then it is empty.     
        }        
        return false;        
    }
    

/** This method checks if the invoking Buffer object is COMPLETELY full, that 
 *  is, if all elements within the BUFFER array contain a Vote class object
 *  reference.
 * 
 * @return - [boolean] Indicating whether the buffer is full or not.
 */    
    public boolean isFull(){
        
        int filledCells = 0;
        
        for(int i = 0; i <= bufferSize - 1; i++){
            if(buffer[i] != null){
                filledCells++;
            }
// For each of the elements in the buffer, check if it is null or not.        
        }
        
        if(filledCells == bufferSize){
            return true;
            
// If there are no null values stored in the buffer, then it is full.
        }        
        return false;
    }
    

/** This method is used to insert a Vote class object into the buffer at position
 *  bufferIN.
 * 
 * @param vote - [Vote] The object to be inserted into the buffer.
 */    
    public void insertItem(Vote vote){
        buffer[bufferIN] = vote;        
        bufferIN = (bufferIN + 1) % bufferSize;                
    }
    

/** This method is used to remove a Vote class object from the buffer at position
 *  bufferOUT. 
 */    
    public Vote removeItem(){ 
        
        Vote item = buffer[bufferOUT];
        
        buffer[bufferOUT] = null;
        bufferOUT = (bufferOUT + 1) % bufferSize;
        
        return item;
    }
    
    
/** This method uses a class Semaphore (mutex) to ENABLE mutual exclusion of the
 *  buffer. 
 */
    public void acquire(){
        mutex.down();        
    }
    

/** This method uses a class Semaphore (mutex) to DISABLE mutual exclusion of the
 *  buffer. 
 */    
    public void release(){
        mutex.up();
    }
    
    
/** This method is used to print out contents of the buffer during the simulation.
 * 
 * @return - [String] The contents (candidateID's) of the votes in the buffer.
 */    
    @Override
    public String toString(){
        
        StringBuilder output = new StringBuilder();
        
        for(Vote currentElement : buffer){
            output.append("["+currentElement+"]");
        }
        
        return output.toString();
    }     
}