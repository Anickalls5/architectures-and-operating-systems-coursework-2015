package Election;
import java.util.Arrays;

/** This is the main class within the Election simulation, the number of 
 *  candidates, voters, booths and officers are given for use in the simulation.
 * 
 * @author Adam R Nickalls, ID: 100088688
 * @version 1.0.
 */
public class Election {
    
    public static Candidate[] candidates;
    public static Booth[] booths;
    public static Officer[] officers;    
    public static int numberOfVoters;
    public static int totalTally = 0;
    
    
/** The method for the simulation itself, this instantiates the appropriate class
 *  objects and executes threads for the Booth and Officer classes.
 * 
 * @param buffer      - [Buffer] The buffer which is shared across the simulation.
 * @param candidateNo - [int] The number of candidates within the simulation.
 * @param voterNo     - [int] The number of votes to be tallied.
 * @param boothNo     - [int] The number of booths within the simulation.
 * @param officerNo   - [int] The number of officers within the simulation.
 */
    public static void runElection(Buffer buffer, int candidateNo, int voterNo, int boothNo, int officerNo){
        
        candidates = new Candidate[candidateNo];
        booths = new Booth[boothNo];
        officers = new Officer[officerNo];
        numberOfVoters = voterNo;
        
        for(int i = candidateNo-1; i >= 0; i--){
            Candidate C = new Candidate(i+1);
            candidates[i] = C;
        }
// The CANDIDATES array is filled with the specified number of Candidate
// class objects.
        
        for(int j = 0; j <= boothNo-1; j++){
            Booth B = new Booth(buffer,j+1);
            booths[j] = B;
            booths[j].start();
        }
// The BOOTHS array is filled with the specified number of Booth class objects,
// which are then executed in their own threads.
        
        for(int k = 0; k <= officerNo-1; k++){
            Officer O = new Officer(buffer,k+1);
            officers[k] = O;
            officers[k].start();
        }
// The OFFICERS array is filled with the specified number of Officer class 
// objects, which are then executed in their own threads.
        
        for(Booth currentElement : booths){
            try{
                currentElement.join();
            }
            catch(InterruptedException iE){
                System.out.println(iE);
            }
        }
// The main thread now waits for all Booth threads to end.
        
        for(Officer currentElement : officers){
            try{
                currentElement.join();
            }
            catch(InterruptedException iE){
                System.out.println(iE);
            }
        }
// The main thread now waits for all Officer threads to end.
    }
    
   
// The main method calls the runElection method after assigning the inputted data
// to the appropriate variables.
    public static void main(String[] args){
        
        System.out.println("");
        
        String C, V, B, O;             
// Temporary variables for the arguments.
        
        int candidateNo, voterNo, boothNo, officerNo;
// Official variables to be used in the simulation.        
        
        try{        
            C = args[0];
            V = args[1];
            B = args[2];
            O = args[3];
            
// If there are no arguments inserted in the command line, then there will be an 
// ArrayIndexOutOfBoundsException thrown, essentially because there is no args
// array present.
        }
        catch(ArrayIndexOutOfBoundsException E){
            C = null;
            V = null;
            B = null;
            O = null;

// If so, then set the temporary string variables to null.            
        }      
        
        if(C == null|| Integer.parseInt(C) <= 0){
            candidateNo = 4;
            System.out.println("CANDIDATE NUMBER IS INVALID AND HAS BEEN SET TO DEFAULT VALUE (4)");
        }
        else{
            candidateNo = Integer.parseInt(args[0]);
        }
        
        if(V == null || Integer.parseInt(V) <= 0){
            voterNo = 10;
            System.out.println("VOTER NUMBER IS INVALID AND HAS BEEN SET TO DEFAULT VALUE (10)");
        }
        else{
            voterNo = Integer.parseInt(args[1]);
        }
        
        if(B == null || Integer.parseInt(B) <= 0){
            boothNo = 1;
            System.out.println("BOOTH NUMBER IS INVALID AND HAS BEEN SET TO DEFAULT VALUE (1)");
        }
        else{
            boothNo = Integer.parseInt(args[2]);            
        }
        
        if(O == null || Integer.parseInt(O) <= 0){
            officerNo = 1;
            System.out.println("OFFICER NUMBER IS INVALID AND HAS BEEN SET TO DEFAULT VALUE (1)");
        }
        else{
            officerNo = Integer.parseInt(args[3]);            
        }

// If any of the arguments are null, then they are set to their default values.
        
        if(officerNo > boothNo){
            boothNo = 1;
            officerNo = 1;
            System.out.println("PLEASE ENSURE THAT THERE ARE FEWER OR AN EQUAL NUMBER OF OFFICERS THAN BOOTHS [DEFAULTS USED]");
        }
// These checks ensure that the parameters are appropriate for the simulation, 
// otherwise, the respective default values are used (4, 10, 1, 1).  Error 
// checking is also used to ensure that the number of officers is smaller than 
// the number of booths. Arguments from the command line are passed into the 
// main method, which are, in turn, passed into the RunElection() method.
        
        System.out.println("");
        System.out.println(">> JAVA ELECTION "+candidateNo+" "+voterNo+" "+boothNo+" "+officerNo);        
        
        int bufferSize = boothNo;        
        Buffer buffer = new Buffer(bufferSize);

        Election.runElection(buffer,candidateNo,voterNo,boothNo,officerNo);
// The parameters are passed onto the simulation, which then begins. The main 
// method waits for this method to finish (the threads are executed in this method)
// before proceeding with the following code.
        
        System.out.println("\nELECTION COMPLETE");
        
        Arrays.sort(candidates);
// The candidates array is sorted in regards to the candidate tallies.
        
        for(int i = 0; i <= candidates.length-1; i++){
            
            if(i == 0 && candidates[0].tally != candidates[1].tally){
                System.out.println(candidates[i]+"\tWINNER");
// If the first element of the now sorted array is not equal to the second,
// then it is a clear victory.            
            }  
            
            else if(i == 0 && candidates[0].tally == candidates[1].tally){
                System.out.println(candidates[0]+"\tJOINT-WINNERS");
// If the first and second elements of the now sorted array are equal, then it
// is a joint victory. This ensures that the first element prints "JOINT-WINNERS".                
            }
            
            else if(i != 0 && candidates[i].tally == candidates[0].tally){
                System.out.println(candidates[i]+"\tJOINT-WINNERS");
// Any subsequent tallies which are equal to the first are also joint-winners.                
            }
            
            else{
                System.out.println(candidates[i]);
// Otherwise, simply print out the candidate details.                
            }
        }
    }   
}    