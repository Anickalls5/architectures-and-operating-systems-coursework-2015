package Election;

/** This class is used to model the candidates of the election simulation.
 * 
 * @author Adam R Nickalls, ID: 100088688
 * @version 1.0.
 */
public class Candidate implements Comparable<Candidate>{
    
    public int candidateID;
    public int tally;

    
/** The Candidate class constructor.
 * 
 * @param id - [int] The candidate's identifier.
 */    
    public Candidate(int id){
        candidateID = id;
        tally = 0;
    }
    

/** This method is used when the simulation ends, it allows the Candidate objects
 *  within the candidates array to be ordered in regards to tally.
 * 
 * @param c - [Candidate] The tally of this is compared to that of the invoking object.
 * @return  - [int] Determines if the invoking object is greater than, less than, or equal to the parameter.
 */    
    @Override
    public int compareTo(Candidate c){
        if(this.tally > c.tally){
            return -1;
        }
        else if(this.tally < c.tally){
            return 1;
        }
        else{
            return 0;
        }
    }
    

/** This method is used to print out the candidate details during the simulation.
 * 
 * @return - [String] The candidateID and tally of the invoking object.
 */     
    @Override
    public String toString(){
        
        StringBuilder output = new StringBuilder();
        
        output.append("Candidate "+candidateID+"\tTally: "+tally);
        
        return output.toString();
    }    
}