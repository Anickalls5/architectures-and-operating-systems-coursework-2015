package Election;

/** This class is used to model the votes of the election simulation.
 * 
 * @author Adam R Nickalls, ID: 100088688
 * @version 1.0.
 */   
public class Vote {

    public int voteID;
    public int candidateID;
    public int boothID;
    

/** The Vote class constructor.
 * 
 * @param voteInput      - [int] The identifier of the Vote object.
 * @param candidateInput - [int] A random candidateID from the candidates array.
 * @param boothInput     - [int] The identifier of the generating Booth object.
 */ 
    public Vote(int voteInput, int candidateInput, int boothInput){
        voteID = voteInput+1;
        candidateID = candidateInput;
        boothID = boothInput;
    }
    

/** This method is used to print out the vote details during the simulation.
 * 
 * @return - [String] The candidateID of the vote.
 */  
    @Override
    public String toString(){
        
        StringBuilder output = new StringBuilder();
        
        output.append(candidateID);
        
        return output.toString();
    }
    
}