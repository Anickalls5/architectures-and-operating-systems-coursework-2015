package Election;
import java.util.Random;

/** This class is used to model voting booths, which spend a random amount of 
 *  time (1 to 10 seconds) generating a Vote class object for use in the Election
 *  simulation.
 * 
 * @author Adam R Nickalls, ID: 100088688
 * @version 1.0.
 */
public class Booth extends Thread{
    
    public int boothID;
    private Buffer sharedBuffer;
    
    
/** The Booth class constructor.
 * 
 * @param b  - [Buffer] This is the buffer that the object is associated to.
 * @param id - [int] The object identifier.
 */
    public Booth(Buffer b, int id){
        sharedBuffer = b;
        boothID = id;
    }   
    
    
/** The main code to be executed for each Booth thread.
 */
    @Override
    public void run(){
        
        Random r = new Random();
               
        if(Election.totalTally == Election.numberOfVoters){            
            while(Booth.activeCount() != 0){
                return;
            }
// When the required number of votes have been recorded, return (end) all 
// currently active Booth threads.
        }
        
        sharedBuffer.acquire();
// This enables the Booth thread to have sole access to the sharedBuffer.

        if(sharedBuffer.isFull() == true){
            sharedBuffer.release();

            try{
                sleep((r.nextInt(9)+1)*1000);
            }
            catch(InterruptedException iE){
                //System.out.println(iE);
            }
// If the buffer is already full, the mutual exclusion to the sharedBuffer is 
// dropped and the thread sleeps for a random time (between 0 and 10 seconds).
        }

        else{
// Otherwise, as long as the buffer is not full, insert an item into it. 
            
            Vote v = new Vote(Election.totalTally,r.nextInt(Election.candidates.length)+1,this.boothID);
            sharedBuffer.insertItem(v);
                       
            System.out.println("VOTER: "+v.voteID+"\tCANDIDATE: "+v.candidateID+"\tBOOTH:   "+v.boothID+"\t[VOTE GENERATED]");
            Election.totalTally++;

            sharedBuffer.release();

// Mutual exclusion to the sharedBuffer is dropped once the vote has been 
// inserted. The Booth thread then sleeps for a random time (1 to 10 seconds).

            try{
                sleep((r.nextInt(9)+1)*1000);
            }
            catch(InterruptedException iE){
                //System.out.println(iE);
            }            
        }
        run();
// Booth threads will be in a collective loop until the particular number of votes 
// has been reached.
    }  
}